# OpenML dataset: Football-striker-performance

https://www.openml.org/d/43401

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The aim of this dataset is to offer in a relatively small number of columns (30) data to compare the performance of some football players, or to compare the efficiency of strikers in-between different European leagues.
Content
Inside the dataset are some performance indicators (goals, assists, minutes played, games played) for football strikers over (up to) the last 5 years.
Acknowledgements
The data was extracted from https://www.transfermarkt.co.uk

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43401) of an [OpenML dataset](https://www.openml.org/d/43401). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43401/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43401/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43401/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

